INSERT INTO vs_users.`user` (created_by,created_date,last_modified_date,version,company_id,first_name,last_name,user_contact_id) VALUES 
(1,'2021-04-13 09:51:52.0','2021-04-13 09:51:52.0',0,3,'Virtuele','Admin',1)
;
INSERT INTO vs_users.user_contact (created_by,created_date,last_modified_date,version,address_line1,address_line2,address_line3,city,contact_type,country,primary_contact_number,primary_email,secondary_contact_number,secondary_email,state,user_id,zip_code) VALUES 
(1,'2021-04-13 09:51:52.0','2021-04-13 09:51:52.0',0,'Opposite to Infosys','#168','Hebbal Industrial Area','Mysore',1,'IN','9887477373','virtuele.admin@gmail.com','8447399393','db.admin@gmail.com',7,1,'84883')
;
INSERT INTO vs_users.user_project (project_id,project_name) VALUES 
(3,'Project 3')
,(2,'Project 2')
,(1,'Project 1')
,(5,'Project 5')
,(4,'Project 4')
;
INSERT INTO vs_users.user_secondary_company (created_by,created_date,last_modified_date,version,company_id,user_id) VALUES 
(1,'2021-04-13 09:51:52.0','2021-04-13 09:51:52.0',0,2,1)
,(1,'2021-04-13 09:51:52.0','2021-04-13 09:51:52.0',0,1,1)
;
INSERT INTO vs_users.user_user_project (user_id,user_project_id) VALUES 
(1,1)
,(1,2)
,(1,3)
,(1,4)
,(1,5)
;
INSERT INTO vs_users.user_user_secondary_company (user_id,user_secondary_company_id) VALUES 
(1,1)
,(1,2)
;