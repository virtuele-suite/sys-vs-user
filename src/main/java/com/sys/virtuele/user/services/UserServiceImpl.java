package com.sys.virtuele.user.services;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.message.ReusableMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.sys.virtuele.user.domain.User;
import com.sys.virtuele.user.domain.UserContact;
import com.sys.virtuele.user.domain.UserSecondaryCompany;
import com.sys.virtuele.user.models.UserCredDto;
import com.sys.virtuele.user.repositories.UserContactRepository;
import com.sys.virtuele.user.repositories.UserRepository;
import com.sys.virtuele.user.repositories.UserSecondaryCompanyRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@ConfigurationProperties(prefix="cred")
public class UserServiceImpl implements UserService {

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	private String username;
	private String password;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserContactRepository userContactRepository;

	@Autowired
	private UserSecondaryCompanyRepository userSecondaryCompanyRepository;

	@Override
	public Optional<User> getUserDetailsByUid(Long userId) {

		log.info(">> userId : " + userId);
		return userRepository.findById(userId);

	}

	@Override
	public User addUser(User user) {

		return userRepository.save(user);
	}

	@Override
	public void deleteUser(Long userId) {

		userRepository.deleteById(userId);

	}

	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public User updateUser(User user) {

		return userRepository.save(user);

	}

	@Override
	public UserContact addUserContact(UserContact userContact) {
		return userContactRepository.save(userContact);
	}

	@Override
	public Optional<User> getUserDetailsByCompanyId(Long companyId) {

		return userRepository.findByCompanyId(companyId);
	}

	@Override
	public UserSecondaryCompany addSecondaryCompany(UserSecondaryCompany userSecondaryCompany) {

		return userSecondaryCompanyRepository.save(userSecondaryCompany);
	}

	@Override
	public Boolean authenticateAndAuthorize(UserCredDto userCredDto) {
		

		String userName = userCredDto.getUserName();
		String password = userCredDto.getPassword();

		log.info(">> userName : " + userName);
		log.info(">> password : " + password);

		
		if (this.username.equals(userName) && this.password.equals(password)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public User getUserDetailsByPrimaryEmailId(String emailId) {

		log.info(">> Geting email based on Primary Email Id");
		
		User returnedUser = userRepository.getByPrimaryEmailId(emailId); 
		
		log.info("<< User Object : " + returnedUser );
		
		return returnedUser;
	}

}
