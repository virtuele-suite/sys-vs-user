package com.sys.virtuele.user.services;

import java.util.List;
import java.util.Optional;

import com.sys.virtuele.user.domain.User;
import com.sys.virtuele.user.domain.UserContact;
import com.sys.virtuele.user.domain.UserSecondaryCompany;
import com.sys.virtuele.user.models.UserCredDto;


public interface UserService {
					
		public Optional<User> getUserDetailsByUid (Long userId);
		public User addUser(User user);
		public void deleteUser(Long userId);
		public List<User> getAllUsers();
		public User updateUser(User user);
		public UserContact addUserContact(UserContact userContact);
		public Optional<User> getUserDetailsByCompanyId (Long companyId);
		public UserSecondaryCompany addSecondaryCompany(UserSecondaryCompany userSecondaryCompany);
		public Boolean authenticateAndAuthorize(UserCredDto userCredDto);
		public User getUserDetailsByPrimaryEmailId (String emailId);

}
