package com.sys.virtuele.user.controllers;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sys.virtuele.user.domain.User;
import com.sys.virtuele.user.domain.UserContact;
import com.sys.virtuele.user.domain.UserSecondaryCompany;
import com.sys.virtuele.user.models.UserAuthResponse;
import com.sys.virtuele.user.models.UserCredDto;
import com.sys.virtuele.user.services.UserService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1/user")
@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	
	@PostMapping("/auth")
	public ResponseEntity<UserAuthResponse> authUser(@RequestBody UserCredDto userCredDto) {
		
		log.info("<< User is being Authenticated !");
		
		UserAuthResponse userAuthResponse = new UserAuthResponse();
		
		Boolean authStatus = userService.authenticateAndAuthorize(userCredDto);
		String userMessage = (authStatus == true ? "User Is Internal and Successfully Autheticated" : "Sorry, User is not found");
		
		
		userAuthResponse.setUserAuthDetails("User Authentication Details");
		userAuthResponse.setUserAuthStatus(authStatus);
		userAuthResponse.setUserAuthToken(UUID.randomUUID().toString());
		userAuthResponse.setUserMessage(userMessage);
		userAuthResponse.setUser(userService.getUserDetailsByPrimaryEmailId(userCredDto.getUserName()));

		HttpHeaders headers = new HttpHeaders();
		
		if(authStatus) {
			headers.add("Location", "/api/v1/user/auth/" + userAuthResponse.getUserAuthToken());
			return new ResponseEntity<UserAuthResponse>(userAuthResponse, HttpStatus.OK);
		}else {
			headers.add("Location", "/api/v1/user/auth/error");
			return new ResponseEntity<UserAuthResponse>(headers, HttpStatus.FORBIDDEN);
		}
	}
	
	@PostMapping
	public ResponseEntity<User> addUser(@RequestBody User user) {
		
		// TODO User user = userMapper.userDtoToUserEntity(userDto);
		
		User savedUser = userService.addUser(user);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", "/api/v1/user/" + savedUser.getId().toString());
		
		log.info("<< Added New User !");
		
		return new ResponseEntity<User>(headers, HttpStatus.CREATED);
	}
	
	
	@PostMapping("/contact")
	public ResponseEntity<UserContact> addUser(@RequestBody UserContact userContact) {
				
		UserContact savedUserContact = userService.addUserContact(userContact);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", "/api/v1/user/" + savedUserContact.getId().toString());
		
		log.info("<< Added New User !");
		
		return new ResponseEntity<UserContact>(headers, HttpStatus.CREATED);
	}
	
	
	@GetMapping("/{userId}")
	public ResponseEntity<User> getUser(@PathVariable("userId") Long userId ) {
		
		log.info(">> "+userId);
		Optional<User> user = userService.getUserDetailsByUid(userId);
		
		log.info("<< Listing User Details !");
		
		return new ResponseEntity<>(user.get(), HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<List<User>> getUser() {
		
		List<User> userList = userService.getAllUsers();
		
		return new ResponseEntity<List<User>>(userList, HttpStatus.OK);
	}
	
	@PutMapping
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateUser(@RequestBody User user) {
		
		User savedUser = userService.updateUser(user);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", "/api/v1/user/" + savedUser.getId().toString());
		
		log.info("<< Updated Existing User !");
		
	} 
	
	@GetMapping("/company/{companyId}")
	public ResponseEntity<User> getUserDetailsByCompanyId(@PathVariable("companyId") Long companyId ) {
		
		log.info(">> "+companyId);
		Optional<User> user = userService.getUserDetailsByCompanyId(companyId);
		
		log.info("<< Listing User Details based on Company Id!");
		
		return new ResponseEntity<>(user.get(), HttpStatus.OK);
	}
	
	
	@PostMapping("/secondary/company")
	public ResponseEntity<UserSecondaryCompany> addSecondaryCompany(@RequestBody UserSecondaryCompany userSecondaryCompany) {
		
		
		UserSecondaryCompany addedCompanyToUser = userService.addSecondaryCompany(userSecondaryCompany);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", "/api/v1/user/secondary/company/" + addedCompanyToUser.getId().toString());
		
		log.info("<< Added New Company to User !");
		
		return new ResponseEntity<UserSecondaryCompany>(headers, HttpStatus.CREATED);
	}
}










