package com.sys.virtuele.user.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author pnc
 *
 */
@Data
@Entity
@NoArgsConstructor
public class UserProject implements Serializable{
	
	/**
	 * Auto Generated serialVersionUid;
	 */
	private static final long serialVersionUID = 4409436497967496807L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Long projectId;
	private String projectName;

}
