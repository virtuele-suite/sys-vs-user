package com.sys.virtuele.user.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.sys.virtuele.common.domain.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class UserContact extends BaseEntity implements Serializable{
	
	
	/**
	 * @author pnc
	 */
	private static final long serialVersionUID = 943927649436079146L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private ContactType contactType;
	private String primaryContactNumber;
	private String secondaryContactNumber;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String city;
	private State state;
	private String zipCode;
	private String country;
	private String primaryEmail;
	private String secondaryEmail;
	
	//@ManyToOne(cascade = CascadeType.ALL)
	// @ManyToOne(cascade = CascadeType.ALL)
	
	private Long userId;
//	@OneToOne(cascade = CascadeType.ALL)
//	private User user;

}
