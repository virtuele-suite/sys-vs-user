package com.sys.virtuele.user.domain;

public enum ContactType {
	COMPANY_CONTACT, USER_CONTACT
}
