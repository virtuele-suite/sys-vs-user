package com.sys.virtuele.user.models;

import lombok.Data;

@Data
public class UserCredDto {
	private String userName;
	private String password;
	private String accessToken;
}
