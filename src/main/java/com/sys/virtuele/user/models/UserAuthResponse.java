package com.sys.virtuele.user.models;

import java.util.Optional;

import com.sys.virtuele.user.domain.User;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserAuthResponse {
	private Boolean userAuthStatus;
	private String userAuthToken;
	private String userAuthDetails;
	private String userMessage;
	private User user;
}
