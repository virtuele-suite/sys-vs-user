package com.sys.virtuele.user.models;

import java.io.Serializable;

import com.sys.virtuele.common.models.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserDto extends BaseDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private Long id;

	private String firstName;
	private String lastName;
}
