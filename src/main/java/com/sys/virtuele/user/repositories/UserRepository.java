package com.sys.virtuele.user.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sys.virtuele.user.domain.User;

@Repository
public interface UserRepository extends JpaRepository <User, Long>{
	
	Optional<User> findById(Long userId) ;
	
	Optional<User> findByCompanyId(Long companyId);
	
	@Query("select u from User u inner join UserContact uc on u.userContact = uc.userId where uc.primaryEmail  = :email_id" )
	User getByPrimaryEmailId(@Param("email_id") String emailId);
	
}
