package com.sys.virtuele.user.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sys.virtuele.user.domain.User;
import com.sys.virtuele.user.domain.UserContact;

@Repository
public interface UserContactRepository extends JpaRepository <UserContact, Long>{
	
	Optional<UserContact> findById(Long userId) ;

	
}
