package com.sys.virtuele.user.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sys.virtuele.user.domain.UserSecondaryCompany;

public interface UserSecondaryCompanyRepository extends JpaRepository <UserSecondaryCompany, Long> {

}
